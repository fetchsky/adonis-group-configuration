"use strict";

const { hooks } = use("@adonisjs/ignitor");
const { ServiceProvider } = use("@adonisjs/fold");

class ConstantProvider extends ServiceProvider {
  register() {
    const config = use("Config").get("group_configuration");
    this.app.singleton("GroupConfiguration", app => {
      const GroupConfiguration = require("../src/GroupConfiguration");
      return new GroupConfiguration(config);
    });
    if (config.loadBeforeHttpServer) {
      hooks.before.httpServer(() => {
        const GroupConfiguration = use("GroupConfiguration");
        GroupConfiguration.load();
      });
    }
    if (config.loadBeforeAceCommand) {
      hooks.before.aceCommand(() => {
        const GroupConfiguration = use("GroupConfiguration");
        GroupConfiguration.load();
      });
    }
  }
}

module.exports = ConstantProvider;
