"use strict";
const path = require("path");

module.exports = async function(cli) {
  try {
    await cli.makeConfig("group_configuration.js", path.join(__dirname, "./templates/group_configuration.mustache"));
    cli.command.completed("create", "config/group_configuration.js");
    await cli.copy(
      path.join(__dirname, "./templates/group_configuration.tmpl"),
      cli.helpers.migrationsPath(new Date().getTime() + "_group_configuration.js")
    );
    cli.command.completed("create", "database/group_configuration.js");
  } catch (error) {
    // ignore errors
  }
};
