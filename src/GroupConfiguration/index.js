"use strict";

const _ = use("lodash");
const db = use("Database");
const moment = use("moment");

class GroupConfiguration {
  constructor(config) {
    this._config = { ...config };
  }

  get loaded() {
    return GroupConfiguration._loaded === true;
  }

  /**
   * This method is load group configurations
   *
   * @param {Boolean} [forced=false]
   * @returns {Promise<Boolean>}
   */
  async load(forced = false) {
    if (!this._config.loadInMemory) {
      return false;
    }
    if (!this.loaded || forced) {
      GroupConfiguration._data = await this.getConfigFromDatabase();
      GroupConfiguration._loaded = true;
    }
    return true;
  }

  /**
   * This method is reload group configurations
   *
   * @returns {Promise<Boolean>}
   */
  async refresh() {
    return this.load(true);
  }

  /**
   * This method is reload group configurations
   *
   * @param {Object} params
   * @param {String} params.config
   */
  async refreshByConfig(params) {
    if (!this._config.loadInMemory) {
      return false;
    }
    let configData = await this.getConfigFromDatabase(params.config);
    _.set(GroupConfiguration._data, params.config, configData[params.config]);
    return true;
  }

  /**
   * This method is reload group configurations
   *
   * @param {Object} params
   * @param {String} params.config
   * @param {String} params.group
   */
  async refreshByGroup(params) {
    if (!this._config.loadInMemory) {
      return false;
    }
    let groupPath = this._getPath(params.config, params.group);
    let groupData = await this.getConfigFromDatabase(params.config, params.group);
    _.set(GroupConfiguration._data, groupPath, _.get(groupData, groupPath));
    return true;
  }

  /**
   * This method is used to load configurations for specified groups
   *
   * @param {String} [config]
   * @param {String} [group]
   * @param {String} [key]
   */
  async getConfigFromDatabase(config, group, key) {
    const data = {};
    const query = db.from(this._config.table);
    if (config) {
      query.where("config", config);
    }
    if (group) {
      query.where("group", group);
    }
    if (key) {
      query.where("key", key);
    }
    const records = await query.where("active", true);
    records.forEach(record => {
      this._addOrRemoveFromCache(_.pick(record, ["config", "group", "key", "value", "data_type"]), false, data);
    });
    return data;
  }

  /**
   * This method is used to get group configuration
   *
   * @param {String} config
   * @param {String} group
   * @param {String} [key]
   * @param {String} [defaultValue=null]
   * @returns {String|Promise}
   */
  get(config, group, key, defaultValue = null) {
    let _path = this._getPath(config, group, key);
    if (this.loaded) {
      return _.get(GroupConfiguration._data, _path, defaultValue);
    }
    return this.getConfigFromDatabase(config, group, key).then(data => {
      return Promise.resolve(_.get(data, _path, defaultValue));
    });
  }

  /**
   * This method is used to set group configuration
   *
   * @param {String} config
   * @param {String} group
   * @param {String} key
   * @param {String} [value]
   * @param {String} [dataType='STRING']
   * @returns {Promise}
   */
  async set(config, group, key, value, dataType = "STRING") {
    if (!_.isEmpty(config) && !_.isEmpty(group) && !_.isEmpty(key)) {
      const date = moment().format("YYYY-MM-DD H:m:s");
      await db.raw(
        "insert into `" +
          this._config.table +
          "` (`config`, `group`, `key`, `value`, `data_type`, `active`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?, ?, true, ?, ?) on duplicate key update `value` = VALUES(`value`), `updated_at` = VALUES(`updated_at`)",
        [config, group, key, value, date, date]
      );
      if (this.loaded) {
        this._addOrRemoveFromCache({ config, group, key, value, data_type: dataType }, false, GroupConfiguration._data);
      }
    }
  }

  async unset(group, key, config = "default", softDelete = true) {
    if (key !== null) {
      if (softDelete) {
        const date = moment().format("YYYY-MM-DD H:m:s");
        await db
          .table(this._config.table)
          .where("key", key)
          .where("group", group)
          .where("config", config)
          .update({
            active: false,
            updated_at: date
          });
      } else {
        await db
          .table(this._config.table)
          .where("key", key)
          .where("group", group)
          .where("config", config)
          .delete();
      }
      if (this.loaded) {
        this._addOrRemoveFromCache({ config, group, key }, true, GroupConfiguration._data);
      }
    }
  }

  /**
   * This method is used to get value path for config
   *
   * @param {String} config
   * @param {String} group
   * @param {String} [key]
   * @returns {String}
   */
  _getPath(config, group, key) {
    let _path = config;
    if (typeof group !== "undefined" && group !== null) {
      _path += "." + group;
    }
    if (typeof key !== "undefined" && key !== null) {
      _path += "." + key;
    }
    return _path;
  }

  /**
   * This method is used to add or remove config from cache
   *
   * @param {Object} params
   * @param {String} params.config
   * @param {String} params.group
   * @param {String} [params.key=null]
   * @param {String} [params.value]
   * @param {String} [params.data_type]
   * @param {Boolean} shouldDelete
   * @param {Object} cacheSource
   */
  _addOrRemoveFromCache(params, shouldDelete, cacheSource) {
    let _path = this._getPath(params.config, params.group, params.key);
    if (shouldDelete) {
      _.unset(cacheSource, _path);
    } else {
      _.set(cacheSource, _path, this._parseValue(params.value, params.data_type));
    }
  }

  /**
   * This method is used to parse value according to the given data type
   *
   * @param {String} value
   * @param {String} [dataType='STRING']
   * @returns {*}
   */
  _parseValue(value, dataType = "STRING") {
    let _value;
    if (dataType === "STRING") {
      _value = value;
    } else if (dataType === "NUMBER") {
      _value = isNaN(value) ? 0 : parseFloat(value);
    } else if (dataType === "BOOLEAN") {
      _value = !value || value.toUpperCase() !== "TRUE" ? false : true;
    } else if (dataType === "DATE") {
      _value = new Date(value);
    } else if (dataType === "JSON") {
      try {
        _value = JSON.parse(value);
      } catch (e) {
        _value = {};
      }
    }
    return _value;
  }
}

GroupConfiguration._loaded = false;
GroupConfiguration._data = {};

module.exports = GroupConfiguration;
